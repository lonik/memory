"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AppComponent = (function () {
    // el:any;
    function AppComponent(elementRef) {
        this.items = ['1', '2', '3', '4', '5', '6', '3', '2', '4', '5', '6', '1'];
        this.num = 1;
        this.count = 0;
        this.searchNumber = [];
        this.winCard = [];
        var nativeElement = core_1.ElementRef;
    }
    AppComponent.prototype.state = function (item, i, event) {
        var el = document.getElementsByName('item');
        el[i].classList.add('flip');
        this.searchNumber.push(item);
        console.log(el[i]);
        this.winCard.push(el[i]);
        if (this.searchNumber.length == 2) {
            this.count++;
            if (this.searchNumber[0] == this.searchNumber[1]) {
                this.winCard[0].classList.remove('flip');
                this.winCard[0].classList.add('show');
                this.winCard[1].classList.remove('flip');
                this.winCard[1].classList.add('show');
                this.winCard = [];
                console.log(this.winCard);
            }
            else {
                this.winCard[0].classList.remove('flip');
                this.winCard[1].classList.remove('flip');
                this.winCard = [];
            }
            this.searchNumber = [];
        }
    };
    ;
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-game',
            templateUrl: 'app/app.component.html'
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map